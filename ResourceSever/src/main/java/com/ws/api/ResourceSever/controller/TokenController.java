package com.ws.api.ResourceSever.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/token")
public class TokenController {

//  @AuthenticationPrincipal used to bind the details of authenticated principle into special Jwt object
    // From Jwt object, we can get the access token itself and all the claims that they provided in the
    // request access token contains
    @GetMapping
    public Map<String, Object> getToken(@AuthenticationPrincipal Jwt jwt){
        return Collections.singletonMap("principal", jwt);
    }

}
